package com.bibao.training.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.training.model.ProfileResponse;
import com.bibao.training.model.ProfilesResponse;
import com.bibao.training.model.TrainingProfile;
import com.bibao.training.service.TrainingProfileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/profile")
@Api(value = "Training Profile Services")
public class TrainingProfileController {
	private static final Logger LOG = LoggerFactory.getLogger(TrainingProfileController.class);
	
	@Autowired
	private TrainingProfileService trainingProfileService;
	
	@PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Create a training profile", response = ProfileResponse.class)
	@ApiResponses(value = {
    		@ApiResponse(code = 200, response = ProfileResponse.class, message = "Successful response"),
    		@ApiResponse(code = 400, message = "Bad request")})
	public ResponseEntity<ProfileResponse> create(@RequestBody TrainingProfile profile) {
		LOG.debug("Received a request to create training profile {}", profile);
		trainingProfileService.createProfile(profile);
		ProfileResponse response = new ProfileResponse();
		response.setMessage("A new training profile is created");
		response.setSuccessful(true);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Get all training profiles", response = ProfilesResponse.class)
	@ApiResponses(value = {
    		@ApiResponse(code = 200, response = ProfilesResponse.class, message = "Successful response"),
    		@ApiResponse(code = 400, message = "Bad request")})
	public ResponseEntity<ProfilesResponse> getAllProfiles() {
		LOG.debug("Received a request to retrieve all training profiles");
		ProfilesResponse response = new ProfilesResponse();
		response.setProfiles(trainingProfileService.getAllProfiles());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/name/{name}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Get a training profile by name", response = ProfileResponse.class)
	@ApiResponses(value = {
    		@ApiResponse(code = 200, response = ProfileResponse.class, message = "Successful response"),
    		@ApiResponse(code = 400, message = "Bad request")})
	public ResponseEntity<ProfileResponse> getProfile(@PathVariable("name") String name) {
		LOG.debug("Received a request to retrieve trainig profile for {}", name);
		ProfileResponse response = trainingProfileService.getProfile(name);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/name/{name}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Delete a training profile by name", response = ProfileResponse.class)
	@ApiResponses(value = {
    		@ApiResponse(code = 200, response = ProfileResponse.class, message = "Successful response"),
    		@ApiResponse(code = 400, message = "Bad request")})
	public ResponseEntity<ProfileResponse> delete(@PathVariable("name") String name) {
		LOG.debug("Received a request to retrieve trainig profile for {}", name);
		ProfileResponse response = trainingProfileService.deactivateProfile(name);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
