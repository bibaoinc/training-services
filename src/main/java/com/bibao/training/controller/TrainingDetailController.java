package com.bibao.training.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.training.model.TrainingDetail;
import com.bibao.training.model.TrainingResponse;
import com.bibao.training.service.TrainingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/detail")
@Api(value = "Training Detail Services")
public class TrainingDetailController {
	private static final Logger LOG = LoggerFactory.getLogger(TrainingDetailController.class);
	
	@Autowired
	private TrainingService trainingService;
	
	@PostMapping(value = "/enroll", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Enroll in a training course", response = TrainingResponse.class)
	@ApiResponses(value = {
    		@ApiResponse(code = 200, response = TrainingResponse.class, message = "Successful response"),
    		@ApiResponse(code = 400, message = "Bad request")})
	public ResponseEntity<TrainingResponse> enroll(@RequestBody TrainingDetail detail) {
		LOG.debug("Received a request to enroll in training {}", detail);
		TrainingResponse response = trainingService.addTrainingCourse(detail);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/name/{name}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Get training detail by name", response = TrainingResponse.class)
	@ApiResponses(value = {
    		@ApiResponse(code = 200, response = TrainingResponse.class, message = "Successful response"),
    		@ApiResponse(code = 400, message = "Bad request")})
	public ResponseEntity<TrainingResponse> getDetail(@PathVariable("name") String name) {
		LOG.debug("Received a request to get training detail for {}", name);
		TrainingResponse response = trainingService.getTrainingDetails(name);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
