package com.bibao.training.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.bibao.training.config.TrainingConfig;

@SpringBootApplication
@Import({TrainingConfig.class})
public class TrainingApp {
	public static void main(String[] args) {
		SpringApplication.run(TrainingApp.class, args);
	}
}
