package com.bibao.training.service;

import com.bibao.training.model.TrainingDetail;
import com.bibao.training.model.TrainingResponse;

public interface TrainingService {
	public TrainingResponse addTrainingCourse(TrainingDetail trainingDetail);
	public TrainingResponse getTrainingDetails(String name);
}
