package com.bibao.training.service;

import java.util.List;

import com.bibao.training.model.ProfileResponse;
import com.bibao.training.model.TrainingProfile;

public interface TrainingProfileService {
	public void createProfile(TrainingProfile profile);
	public List<TrainingProfile> getAllProfiles();
	public ProfileResponse getProfile(String name);
	public ProfileResponse deactivateProfile(String name);
}
