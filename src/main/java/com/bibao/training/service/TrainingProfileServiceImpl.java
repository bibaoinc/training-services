package com.bibao.training.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bibao.training.dynamodb.dao.TrainingProfileDao;
import com.bibao.training.dynamodb.entity.TrainingProfileEntity;
import com.bibao.training.model.ProfileResponse;
import com.bibao.training.model.TrainingProfile;

@Service
public class TrainingProfileServiceImpl implements TrainingProfileService {
	@Autowired
	private TrainingProfileDao trainingProfileDao;
	
	@Override
	public void createProfile(TrainingProfile profile) {
		TrainingProfileEntity entity = this.mapToTrainingProfileEntity(profile);
		trainingProfileDao.createTrainingProfile(entity);
	}

	@Override
	public List<TrainingProfile> getAllProfiles() {
		List<TrainingProfileEntity> entities = trainingProfileDao.getAllTrainingProfiles();
		return entities.stream().map(this:: mapToTrainingProfile).collect(Collectors.toList());
	}
	
	@Override
	public ProfileResponse getProfile(String name) {
		TrainingProfileEntity entity = trainingProfileDao.getTrainingProfile(name);
		ProfileResponse response = new ProfileResponse();
		if (entity!=null) {
			response.setProfile(this.mapToTrainingProfile(entity));
			response.setMessage("Successfully retrieved the training profile for student " + name);
			response.setSuccessful(true);
		} else {
			response.setMessage("No training profile found for student " + name);
			response.setSuccessful(false);
		}
		return response;
	}
	
	@Override
	public ProfileResponse deactivateProfile(String name) {
		boolean deactivate = trainingProfileDao.deactivateTrainigProfile(name);
		ProfileResponse response = new ProfileResponse();
		if (deactivate) {
			response.setMessage("Successfully deactivate training profile for student " + name);
			response.setSuccessful(true);
		} else {
			response.setMessage("The training profile for student " + name + " is not found");
			response.setSuccessful(false);
		}
		return response;
	}
	
	private TrainingProfileEntity mapToTrainingProfileEntity(TrainingProfile profile) {
		TrainingProfileEntity entity = new TrainingProfileEntity();
		entity.setPK(TrainingProfileEntity.DEFAULT_PK);
		entity.setSK(profile.getStudentId());
		entity.setStudentId(profile.getStudentId());
		entity.setStudentName(profile.getStudentName());
		entity.setAddress(profile.getAddress());
		entity.setActive(true);
		entity.setCreateTimestamp(new Date());
		return entity;
	}

	private TrainingProfile mapToTrainingProfile(TrainingProfileEntity entity) {
		TrainingProfile profile = new TrainingProfile();
		profile.setAddress(entity.getAddress());
		profile.setStudentId(entity.getStudentId());
		profile.setStudentName(entity.getStudentName());
		profile.setCreateTimestamp(entity.getCreateTimestamp());
		return profile;
	}

}
