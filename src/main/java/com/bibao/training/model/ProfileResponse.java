package com.bibao.training.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfileResponse {
	private String message;
	private boolean successful;
	private TrainingProfile profile;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isSuccessful() {
		return successful;
	}
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
	public TrainingProfile getProfile() {
		return profile;
	}
	public void setProfile(TrainingProfile profile) {
		this.profile = profile;
	}
}
