package com.bibao.training.model;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TrainingDetail {
	private String studentName;
	private String course;
	private BigDecimal trainingFee;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
	private Date createTimestamp;
	
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public BigDecimal getTrainingFee() {
		return trainingFee;
	}
	public void setTrainingFee(BigDecimal trainingFee) {
		this.trainingFee = trainingFee;
	}
	public Date getCreateTimestamp() {
		return createTimestamp;
	}
	public void setCreateTimestamp(Date createTimestamp) {
		this.createTimestamp = createTimestamp;
	}
	
	@Override
	public String toString() {
		return "TrainingDetail [studentName=" + studentName + ", course=" + course + ", trainingFee=" + trainingFee
				+ ", createTimestamp=" + createTimestamp + "]";
	}
	
}
