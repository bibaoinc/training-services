package com.bibao.training.model;

import java.util.List;

public class ProfilesResponse {
	private List<TrainingProfile> profiles;

	public List<TrainingProfile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<TrainingProfile> profiles) {
		this.profiles = profiles;
	}
	
}
