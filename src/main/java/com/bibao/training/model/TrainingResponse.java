package com.bibao.training.model;

import java.util.List;

import com.bibao.training.dynamodb.entity.Address;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainingResponse {
	private String message;
	private boolean successful;
	private String studentId;
	private String studentName;
	private Address address;
	private List<TrainingDetail> trainingDetails;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isSuccessful() {
		return successful;
	}
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<TrainingDetail> getTrainingDetails() {
		return trainingDetails;
	}
	public void setTrainingDetails(List<TrainingDetail> trainingDetails) {
		this.trainingDetails = trainingDetails;
	}
}
