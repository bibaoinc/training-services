package com.bibao.training.dynamodb.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import org.apache.commons.lang3.BooleanUtils;

public class BooleanToStringConverter implements DynamoDBTypeConverter<String, Boolean> {
	@Override
	public String convert(Boolean bool) {
		return BooleanUtils.toString(bool, "Y", "N");
	}

	@Override
	public Boolean unconvert(String object) {
		return BooleanUtils.toBoolean(object, "Y", "N");
	}
}
