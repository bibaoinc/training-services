package com.bibao.training.dynamodb.dao;

import java.util.List;

import com.bibao.training.dynamodb.entity.TrainingEntity;

public interface TrainingDao {
	public void enrollTraining(TrainingEntity entity);
	public List<TrainingEntity> getTrainings(String name);
}
