package com.bibao.training.dynamodb.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTableMapper;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.bibao.training.dynamodb.entity.TrainingEntity;

@Repository
public class TrainingDaoImpl implements TrainingDao {
	@Autowired
	private DynamoDBTableMapper<TrainingEntity, String, String> trainingEntityMapper;
	
	@Override
	public void enrollTraining(TrainingEntity entity) {
		trainingEntityMapper.save(entity);
	}

	@Override
	public List<TrainingEntity> getTrainings(String name) {
		Map<String, String> attributeNames = new HashMap<>();
		attributeNames.put("#PK", "PK");
		attributeNames.put("#studentName", "studentName");
		Map<String, AttributeValue> attributeValues = new HashMap<>();
		attributeValues.put(":PK", new AttributeValue().withS(TrainingEntity.DEFAULT_PK));
		attributeValues.put(":studentName", new AttributeValue().withS(name));
		DynamoDBQueryExpression<TrainingEntity> queryExpression = 
				new DynamoDBQueryExpression<TrainingEntity>()
				.withKeyConditionExpression("#PK = :PK")
				.withFilterExpression("#studentName = :studentName")
				.withExpressionAttributeNames(attributeNames)
				.withExpressionAttributeValues(attributeValues)
				.withScanIndexForward(false);
		return trainingEntityMapper.query(queryExpression);
	}

}
