package com.bibao.training.dynamodb.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTableMapper;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.bibao.training.dynamodb.entity.TrainingProfileEntity;

@Repository
public class TrainingProfileDaoImpl implements TrainingProfileDao {
	private static final Logger LOG = LoggerFactory.getLogger(TrainingProfileDaoImpl.class);
	
	@Autowired
	DynamoDBTableMapper<TrainingProfileEntity, String, String> trainingProfileEntityMapper;
	
	@Override
	public void createTrainingProfile(TrainingProfileEntity entity) {
		LOG.debug("Profile saved to DynamoDB: {}", entity);
		trainingProfileEntityMapper.save(entity);
	}

	@Override
	public boolean deactivateTrainigProfile(String name) {
		TrainingProfileEntity entity = getTrainingProfile(name);
		if (entity==null) return false;
		entity.setActive(false);
		trainingProfileEntityMapper.save(entity);
		return true;
	}

	@Override
	public TrainingProfileEntity getTrainingProfile(String name) {
		Map<String, String> attributeNames = new HashMap<>();
		attributeNames.put("#PK", "PK");
		attributeNames.put("#studentName", "studentName");
		attributeNames.put("#active", "active");
		Map<String, AttributeValue> attributeValues = new HashMap<>();
		attributeValues.put(":PK", new AttributeValue().withS(TrainingProfileEntity.DEFAULT_PK));
		attributeValues.put(":studentName", new AttributeValue().withS(name));
		attributeValues.put(":active", new AttributeValue().withS("Y"));
		DynamoDBQueryExpression<TrainingProfileEntity> queryExpression = 
				new DynamoDBQueryExpression<TrainingProfileEntity>()
				//.withIndexName("GSI")
				.withKeyConditionExpression("#PK = :PK")
				.withFilterExpression("#studentName = :studentName and #active = :active")
				.withExpressionAttributeNames(attributeNames)
				.withExpressionAttributeValues(attributeValues)
				.withLimit(1);
		List<TrainingProfileEntity> entities = trainingProfileEntityMapper.query(queryExpression);
		if (CollectionUtils.isNotEmpty(entities)) return entities.get(0);
		else return null;
	}

	@Override
	public List<TrainingProfileEntity> getAllTrainingProfiles() {
		TrainingProfileEntity entity = new TrainingProfileEntity();
		entity.setPK(TrainingProfileEntity.DEFAULT_PK);
		DynamoDBQueryExpression<TrainingProfileEntity> queryExpression = new DynamoDBQueryExpression<>();
		queryExpression.setHashKeyValues(entity);
		return trainingProfileEntityMapper.query(queryExpression);
	}

}
