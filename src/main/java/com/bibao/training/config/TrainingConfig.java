package com.bibao.training.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTableMapper;
import com.bibao.training.dynamodb.entity.TrainingEntity;
import com.bibao.training.dynamodb.entity.TrainingProfileEntity;

@Configuration
@ComponentScan(basePackages = "com.bibao.training")
public class TrainingConfig {
	@Bean(name="amazonDynamoDB")
	public AmazonDynamoDB getAmazonDynamoDB() {
		return AmazonDynamoDBClientBuilder.standard().build();
		//return AmazonDynamoDBClientBuilder.defaultClient();
	}
	
	@Bean
    public DynamoDBMapper dynamoDBMapper(@Qualifier("amazonDynamoDB") AmazonDynamoDB dynamoDb) {
        return new DynamoDBMapper(dynamoDb);
    }
	
	@Bean("trainingProfileEntityMapper")
    public DynamoDBTableMapper<TrainingProfileEntity, String, String> trainingProfileEntityMapper(DynamoDBMapper dynamoDBMapper) {
        return dynamoDBMapper.newTableMapper(TrainingProfileEntity.class);
    }
	
	@Bean("trainingEntityMapper")
    public DynamoDBTableMapper<TrainingEntity, String, String> trainingEntityMapper(DynamoDBMapper dynamoDBMapper) {
        return dynamoDBMapper.newTableMapper(TrainingEntity.class);
    }
}
