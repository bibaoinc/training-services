package com.bibao.training.controller;

import java.util.Arrays;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.bibao.training.dynamodb.dao.TrainingProfileDaoImpl;
import com.bibao.training.dynamodb.entity.Address;
import com.bibao.training.dynamodb.entity.TrainingProfileEntity;
import com.bibao.training.model.TrainingProfile;

public class TrainingProfileControllerTest extends AbstractMockMvcTest {
	private static final String URL = "/profile";
	
	@MockBean
	private TrainingProfileDaoImpl trainingProfileDao;
	
	@Test
	public void testCreate() throws Exception {
		String requestUrl = URL + "/create";
		
		TrainingProfile request = new TrainingProfile();
		request.setStudentId("123");
		request.setStudentName("test");
		request.setCreateTimestamp(new Date());
		Mockito.doNothing().when(trainingProfileDao)
				.createTrainingProfile(ArgumentMatchers.any(TrainingProfileEntity.class));
		ResultActions resultActions = mvc.perform(MockMvcRequestBuilders
										.post(requestUrl)
										.contentType(MediaType.APPLICATION_JSON)
										.content(asJsonString(request))
										.accept(MediaType.APPLICATION_JSON))
										.andExpect(MockMvcResultMatchers.status().isOk());
		
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("A new training profile is created"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.successful").value(true));
		
		Mockito.verify(trainingProfileDao).createTrainingProfile(ArgumentMatchers.any(TrainingProfileEntity.class));
	}
	
	@Test
	public void testGetAllProfiles() throws Exception {
		String requestUrl = URL + "/all";
		
		Mockito.when(trainingProfileDao.getAllTrainingProfiles())
				.thenReturn(Arrays.asList(this.createEntity()));
		ResultActions resultActions = mvc.perform(MockMvcRequestBuilders
										.get(requestUrl)
										.contentType(MediaType.APPLICATION_JSON)
										.accept(MediaType.APPLICATION_JSON))
										.andExpect(MockMvcResultMatchers.status().isOk());
		
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profiles[0].studentId").value("555"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profiles[0].studentName").value("Linda"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profiles[0].address.street").value("100 Main Street"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profiles[0].address.city").value("NYC"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profiles[0].address.state").value("NY"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profiles[0].address.zipCode").value("12345"));
		
		Mockito.verify(trainingProfileDao).getAllTrainingProfiles();
	}
	
	@Test
	public void testGetProfile() throws Exception {
		String requestUrl = URL + "/name/Linda";
		
		Mockito.when(trainingProfileDao.getTrainingProfile(ArgumentMatchers.eq("Linda")))
				.thenReturn(this.createEntity());
		ResultActions resultActions = mvc.perform(MockMvcRequestBuilders
										.get(requestUrl)
										.contentType(MediaType.APPLICATION_JSON)
										.accept(MediaType.APPLICATION_JSON))
										.andExpect(MockMvcResultMatchers.status().isOk());
		
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Successfully retrieved the training profile for student Linda"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.successful").value(true));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profile.studentId").value("555"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profile.studentName").value("Linda"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profile.address.street").value("100 Main Street"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profile.address.city").value("NYC"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profile.address.state").value("NY"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.profile.address.zipCode").value("12345"));
		
		Mockito.verify(trainingProfileDao).getTrainingProfile(ArgumentMatchers.eq("Linda"));
		
		// No record found
		Mockito.when(trainingProfileDao.getTrainingProfile(ArgumentMatchers.eq("Linda")))
				.thenReturn(null);
		mvc.perform(MockMvcRequestBuilders
			.get(requestUrl)
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("No training profile found for student Linda"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.successful").value(false));
	}
	
	@Test
	public void testDelete() throws Exception {
		String requestUrl = URL + "/name/Linda";
		
		Mockito.when(trainingProfileDao.deactivateTrainigProfile(ArgumentMatchers.eq("Linda")))
				.thenReturn(true);
		mvc.perform(MockMvcRequestBuilders
			.delete(requestUrl)
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Successfully deactivate training profile for student Linda"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.successful").value(true));
		
		Mockito.verify(trainingProfileDao).deactivateTrainigProfile(ArgumentMatchers.eq("Linda"));
		
		// No record is deleted
		Mockito.when(trainingProfileDao.deactivateTrainigProfile(ArgumentMatchers.eq("Linda")))
			.thenReturn(false);
		mvc.perform(MockMvcRequestBuilders
			.delete(requestUrl)
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("The training profile for student Linda is not found"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.successful").value(false));
	}
	
	private TrainingProfileEntity createEntity() {
		TrainingProfileEntity entity = new TrainingProfileEntity();
		entity.setStudentId("555");
		entity.setStudentName("Linda");
		Address address = new Address();
		address.setStreet("100 Main Street");
		address.setCity("NYC");
		address.setState("NY");
		address.setZipCode("12345");
		entity.setAddress(address);
		return entity;
	}
}
